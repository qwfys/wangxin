package org.qwfys.app.wangxin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WangxinApplication {

	public static void main(String[] args) {
		SpringApplication.run(WangxinApplication.class, args);
	}
}
